#!/usr/bin/perl
use warnings;
use strict;



print "\nwrktr4k.pl activated. running script...\n\n";



my $datafile = 'workdata.txt';
my $themefile = 'template.html';
my $targetfile = 'example.html';



my @lines = ();
my $tabs = "";
my $ostr = "";



print "global vars set.\n";
print "opening data-file $datafile...\n";

open my $IN, '<', $datafile or die "error: can't open data-file $datafile: $!\n";

my $lnum = 0;
my $gotprefs = 0;
while (my $rowstr = <$IN>) { # For every line in the data-file...
  
  chomp $rowstr; # Remove newline-character from the "rowstr" string
  $rowstr =~ s/^\s*(.*?)\s*$/$1/; # Remove all whitespace from the left and right sides of the "rowstr" string
  
  my @row = split / /, $rowstr; # Split the row by whitespace, and turn it into an array
  
  if ((scalar @row) == 0) { # If this line contains no items...
    next; # Go to the while-loop's next iteration, without increasing $lnum
  }
  
  # At this point, it is confirmed that @row contains at least one item.
  # If this is the first line with any items, then it must be the prefs-line, and will therefore be parsed as such.
  
  if ($gotprefs == 0) { # If the prefs-line has not yet been received...
    $tabs = "\t" x $row[0]; # Get the user-defined number of tabs to put at the start of each line
    $gotprefs = 1; # Set a flag for "the prefs-line has been received"
    next; # Go to the while-loop's next iteration, without increasing $lnum
  }
  
  $_ = lc $row[0]; # Get the lowercase version of the first item in the line, into the default var specifically
  
  if (m/note/) { # If the first item in the line is "note" (of any capitalization)...
    splice @row, 0, 1; # Remove that item from the line
    $lines[$lnum] = $tabs . "<note>" . join(' ', @row) . "</note>"; # Format this line in the "note"-style
    $lnum++; # Increase the "line number" var
    next; # Go to the while-loop's next iteration
  }
  
  if ((scalar @row) == 1) { # If this line contains only one item...
    
    # For any item starting with a number, format it like a year; else format it like a month
    $lines[$lnum] = $tabs . "<p class=\"" . (/\d+/ ? "year" : "month") . "\">" . $row[0] . "</p>";
    
    $lnum++; # Increase the "line number" var
    next; # Go to the while-loop's next iteration
    
  }
  
  # Having checked for empty lines, the setting-line, note-lines, month-lines, and year-lines,
  # the only remaining valid possibility is a work-line, which should start with an hours-number. So...
  
  # If this line's first item contains any non-numerics, or its numerical value is >24...
  if (m/\D/ || ($_ > 24)) {
    next; # Go to the while-loop's next iteration, without increasing $lnum
  }
  
  # Set this work-line's outgoing-line to contain the opening tag for a workday with its given number of hours
  $lines[$lnum] = $tabs . "<p class=\"w" . $_ . "\">";
  
  splice @row, 0, 1; # Remove the "work-hours" entry from the start of the work-line
  
  foreach $_ (@row) { # For each item in the work-line that has not yet been processed...
    
    if (m/^(\d+)([A-Za-z]+$)/) { # If this item contains a "numbers-then-letters" character sequence...
      
      # Assume this item is a day's designator in "daymonth" format (e.g. 12jun), and format it as such
      $lines[$lnum] .= $_ . " ";
      
    } elsif (m/^([A-Za-z]{1,3})[A-Za-z]*(\d*)$/) { # Else, if this item is "letters-then-numbers" or "letters-only"...
      
      # Assume this item is a tag-name, and format it as such
      $lines[$lnum] .= "<" . $1 . $2 . "/>";
      
    }
    
  }
  
  $lines[$lnum] .= "</p>"; # End the current line's paragraph-tag, and insert a newline
  
  $lnum++; # Increase the number that tracks which entry of @lines to put new data into
  
}

close $IN; # Close the data-file

print "$datafile parsed and closed.\n";

print "opening theme-file $themefile...\n";

open my $IN2, '<', $themefile or die "error: can't open theme-file $themefile: $!\n";

while (my $thstr = <$IN2>) { # For every line in the theme-file...
  
  if (index($thstr, "<!-- wrktr4k -->") != -1) { # If this line contains wrktr4k's activation-phrase...
    $ostr .= (join "\n", @lines) . "\n"; # Add all of the processed lines from the datafile-related routines + final \n
  } else { # Else, if this line doesn't contain wrktr4k's activation-phrase...
    $ostr .= $thstr; # Add the line to the output-string without any modifications
  }
  
}

close $IN2; # Close the theme-file

print "$themefile parsed and closed.\n";

print "creating target-file $targetfile...\n";

open my $OUT, '>', $targetfile or die "error: can't open target-file $targetfile: $!\n";

print $OUT $ostr; # Print the entire output-string to the newly-created target-file

close $OUT; # Close the target-file

print "$targetfile created, populated, and closed.\n\n";
print "wrktr4k processing done!\n\n";
print "$targetfile is now ready to publish.\n\n";



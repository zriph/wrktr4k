




# Notes-to-self:
- `note Text of the note-to-self goes here!`
- These will show up between your work-lines, wherever you put them.
- Each note-to-self occupies the entire screen's width, and will create a gap between work-entries. It is therefore best-practice to put all your notes-to-self in clusters at the end of a given month, or in a single cluster at the top of your data-file.
- *Important:* These will be displayed on your main wrktr4k page! If you think you ever might upload your wrktr4k page online, then you should not use this feature for any notes-to-self that you want to keep private.



# Work-entry components:

## Valid format
A valid work-line is in the following format: `5 acc3 morale3 eff3 task3 21feb art coding`
We can pick it apart in the following way:
- `5`: This is the number of hours you worked that day. It must always be the first item on the line.
- `accom3`: "accomplishment sensation" (detailed below)
- `morale3`: "morale" (detailed below)
- `eff3`: "efficiency" (detailed below)
- `task3`: "task importance" (detailed below)
- `21feb`: "day-and-month" (optional, but good for keeping organized)
- `art coding` "task types" (detailed below)
- (not shown in example) `injured` etc.: "status effects" (detailed below)

## Accomplishment sensation: Do I feel like I've accomplished something today?
- `accom5` very accomplished
- `accom4` accomplished
- `accom3` sort of accomplished
- `accom2` hardly at all
- `accom1` dismal

## Morale: How is my whole-life morale doing?
- `morale5` excellent morale
- `morale4` good morale
- `morale3` average morale
- `morale2` bad morale
- `morale1` awful morale

## Efficiency: Am I on a roll, or struggling to finish simple tasks?
- `eff5` on a roll!
- `eff4` doing decently
- `eff3` average efficiency
- `eff2` sort of blocked
- `eff1` struggling

## Task importance: Did I do main tasks, side-tasks, or something inbetween?
- `task5`  main tasks
- `task4`  main-ish tasks
- `task3`  main-and-side tasks
- `task2`  side-ish tasks
- `task1`  side tasks

## Task types: For tracking the types of tasks you've done on a given day.
- `art`     🎨
- `chores`  🧹
- `coding`  💻
- `music`   🎵
- `other`   🛠️
- `writing` ✒️

## Status effects:
- `injured`  🤕
- `sick`     😷
- `verysick` 🤢

## Other markers:
- New year markers: `2023` etc.
- New month markers: `JAN`, `Jan`, `jan`, etc.
- Each of these marker-types must be the only item on their line.
